import java.util.Scanner;

class CarnivorousCheo{
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan banyaknya porsi makanan: ");
        int porsi = input.nextInt();

        for (int x = 1 ; x <= porsi ; x++){
            if(x % 3 == 0 && x % 2 == 0) System.out.println("Makanan campuran sayur dan daging");
            else if(x % 3 == 0) System.out.println("Makanan daging");
            else System.out.println("Makanan sayur");
        }
    }
}